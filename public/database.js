var mongoose = require('mongoose');
var async = require('async');


var database = require('./schema');

var kitty1 = new database.Kitty({
    startNumber: 1,
    name: 'Snowball',
});
var kitty2 = new database.Kitty({
    startNumber: 2,
    name: 'Tosia',
});
var kitty3 = new database.Kitty({
    startNumber: 3,
    name: 'Pixel',
});
var kitty4 = new database.Kitty({
    startNumber: 4,
    name: 'Fluffy',
});
var kitty5 = new database.Kitty({
    startNumber: 5,
    name: 'Evian',
});
var kitty6 = new database.Kitty({
    startNumber: 6,
    name: 'Mirabel',
});
var kitty7 = new database.Kitty({
    startNumber: 7,
    name: 'Kolumbia',
});
var kitty8 = new database.Kitty({
    startNumber: 8,
    name: 'Puszek',
});
var kitty9 = new database.Kitty({
    startNumber: 9,
    name: 'Mruczek',
});
var kitty10 = new database.Kitty({
    startNumber: 10,
    name: 'Sliwa',
});

var judge1 = new database.Judge({
    judgeNumber: 1,
    haslo: 1234
});
var judge2 = new database.Judge({
    judgeNumber: 2,
    haslo: 1235
});
var judge3 = new database.Judge({
    judgeNumber: 3,
    haslo: 1236
});
var judge4 = new database.Judge({
    judgeNumber: 4,
    haslo: 1237
});
var judge5 = new database.Judge({
    judgeNumber: 5,
    haslo: 1238
});
var judge6 = new database.Judge({
    judgeNumber: 6,
    haslo: 1239
});
var judge7 = new database.Judge({
    judgeNumber: 7,
    haslo: 1220
});
var judge8 = new database.Judge({
    judgeNumber: 8,
    haslo: 1221
});

var judge9 = new database.Judge({
    judgeNumber: 9,
    haslo: 1222
});

var judge10 = new database.Judge({
    judgeNumber: 10,
    haslo: 1223
});


var kittySave = function(kitty) {
    return function(kittySaveDone) {
        kitty.save(function(err) {
            if (err) {
                console.log(err);
            }
            kittySaveDone(err);

        });
    };
};


var judgeSave = function(judge) {
    return function(judgeSaveDone) {
        judge.save(function(err) {
            if (err) {
                console.log(err);
            }
            judgeSaveDone(err);

        });
    };
};
var save1 = kittySave(kitty1);
var save2 = kittySave(kitty2);
var save3 = kittySave(kitty3);
var save4 = kittySave(kitty4);
var save5 = kittySave(kitty5);
var save6 = kittySave(kitty6);
var save7 = kittySave(kitty7);
var save8 = kittySave(kitty8);
var save9 = kittySave(kitty9);
var save10 = kittySave(kitty10);

var savej1 = judgeSave(judge1);
var savej2 = judgeSave(judge2);
var savej3 = judgeSave(judge3);
var savej4 = judgeSave(judge4);
var savej5 = judgeSave(judge5);
var savej6 = judgeSave(judge6);
var savej7 = judgeSave(judge7);
var savej8 = judgeSave(judge8);
var savej9 = judgeSave(judge9);
var savej10 = judgeSave(judge10);

var exit = function(cb) {
    process.exit(0, function(){
        cb();
    });
};

var connect = function(openDone) {
    database.db.once('open', function() {
        openDone();
    });
};



var insertKitty = function(cb) {
    async.parallel([save1, save2, save3, save4, save5, save6, save7, save8, save9, save10], function(err) {
        if (err) {
            console.log(err);
        } else {
            cb();
        }
    });
};

var insertJudge = function(cb) {
    async.parallel([savej1, savej2, savej3, savej4, savej5, savej6, savej7, savej8, savej9, savej10], function(err) {
        if (err) {
            console.log(err);
        } else {
            cb();
        }
    });
};

async.series([connect, insertKitty, insertJudge,exit]);
