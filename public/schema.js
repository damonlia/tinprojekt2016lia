var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));


var kittySchema = mongoose.Schema({
    startNumber: Number,
    name: String,
});

var judgeSchema = mongoose.Schema({
    judgeNumber: Number,
    haslo: Number
});

var resultsSchema = mongoose.Schema({
    kot_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Kitty'
    },
    sedzia_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Judge'
    },
    siersc: Number,
    pazury: Number,
    uszy: Number,
    postawa: Number,
    uzebienie: Number
});

var Kitty = mongoose.model('Kitty', kittySchema);
var Judge = mongoose.model('Judge', judgeSchema);
var Result = mongoose.model('Result', resultsSchema);

exports.Judge = Judge;
exports.Kitty = Kitty;
exports.Result = Result;
exports.db =db;
