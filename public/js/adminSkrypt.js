/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false, $:false */
"use strict";

$(function(event) {
    var socketIdAdmina;
    var socket;
    var start = $('#start');
    var finish = $('#finish');
    if (!socket || !socket.connected) {
        socket = io({
            forceNew: true
        });
    }

    socket.on('connect', function() {
        // socket.emit('dajsocketid');
        console.log('Nawiązano połączenie przez Socket.io');
    });

    socket.on('pospieszenie', function() {
        $('body').append("<p> Nie wszyscy wybrali oceny !! Pokaz nie może się zakończyć</p>");
    });
    start.click(function() {
        console.log("kliknieto start");
        socket.emit('rozpocznijPokaz');
    });


    finish.click(function() {
        console.log("kliknieto zakoncz ");
        socket.emit('zakonczPokaz');
    });



});
