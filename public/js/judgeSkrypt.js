/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false, $:false, _:false */
"use strict";

$(function(event) {
    var siersc = $("#siersc");
    var pazury = $("#pazury");
    var uszy = $("#uszy");
    var postawa = $("#postawa");
    var uzebienie = $("#uzebienie");
    var socket;
    var hasloInput = $('#inputLogin');
    var blad = $('#blad');
    var zaloguj = $('#buttonLogin');
    var idsedziego;
    var idkota;
    var poleWSchemacie;
    var sierscVal,
        pazuryVal,
        uszyVal,
        postawaVal,
        uzebienieVal,noSedziego;
    var tablicaWynikow = [];
    if (!socket || !socket.connected) {
        socket = io({
            forceNew: true
        });
    }

    socket.on('connect', function() {
        console.log('Nawiązano połączenie przez Socket.io');
    });

    zaloguj.click(function() {
        console.log("dotarlo do klikniecia zaloguj");
        socket.emit('sprawdzHaslo', parseInt(hasloInput.val()));
    });

    socket.on('bledneHaslo', function(data) {
        blad.text(data);
    });

    socket.on('otworzFormularz', function(nrSedziego, idObecnegoSedziego, idObecnegoKota) {
        console.log("dotarlo do otworzenia formularza");
        idsedziego = idObecnegoSedziego;
        idkota = idObecnegoKota;
        $('#divLogin').hide();
        $('#form').show();
        $('#nrS').text(nrSedziego);
        zablokujFormularz();
        console.log("blokuje formularz");
    });
    $(window).on("beforeunload", function() {
        socket.emit('usunSedziegoZListyZalogowanych');
    });

    $('#siersc').bind('click', function() {
        pobierzWartosc($(this).val(), 'sd', 'siersc');
    });
    $('#pazury').bind('click', function() {
        pobierzWartosc($(this).val(), 'pad', 'pazury');
    });
    $('#uszy').bind('click', function() {
        pobierzWartosc($(this).val(), 'usd', 'uszy');
    });
    $('#postawa').bind('click', function() {
        pobierzWartosc($(this).val(), 'pod', 'postawa');
    });
    $('#uzebienie').bind('click', function() {
        pobierzWartosc($(this).val(), 'uzd', 'uzebienie');
    });
    var pobierzWartosc = function(poleVal, idDiv, poleWSchemacie) {
        $('#' + idDiv).text(poleVal);
        socket.emit('updateDoBazy', poleVal, idsedziego, idkota, poleWSchemacie);
        console.log("PoleVal: " + poleVal + "idkota: " + idkota + "idsedziego: " + idsedziego);
    };
    /////////////////////////////////////////////////////////////////
    var zablokujFormularz = function() {
        $('.kot').prop('disabled', true);
        $('#czekaj').show();
    };
    var udostepnijFormularz = function() {
        $('.kot').prop('disabled', false);
        $('#czekaj').hide();

    };
    socket.on('zablokujFormularz', function() {
        zablokujFormularz();
        $( "#siersc" ).slider().slider('value','refresh');

    });
    socket.on('nastepnyKot', function(idNastepnegoKota) {
        idkota = idNastepnegoKota;
    });
    socket.on('udostepnijFormularz', function(nrStartowy) {
        console.log("udostepniono formularz");
        udostepnijFormularz();
        console.log(nrStartowy);
        $('#nrK').text(nrStartowy);
    });
    socket.on('pospieszenie', function() {
        $('body').append("<p id='p'> Wybierz ocene dla kazdej kategorii!!</p>");
    });
    socket.on('wyswietlTabele', function(obiektWyniki) {
        //$('#p').hide();
        console.log("dotarło do wyswietlenia wynikow");
        tablicaWynikow.push(obiektWyniki);
        $('tbody').empty();
        tablicaWynikow.sort(function(a, b) {
            if (a.sumWszystko < b.sumWszystko) {
                return 1;
            }
            if (a.sumWszystko > b.sumWszystko) {
                return -1;
            }
            if (a.sumSiersc < b.sumSiersc) {
                return 1;
            }
            if (a.sumSiersc > b.sumSiersc) {
                return -1;
            }
        });
        _.each(tablicaWynikow, function(wynik, idx) {
            $('tbody').append("<tr><td>" + (idx + 1) + " </td><td>" + wynik.imieKota + " </td><td>" + wynik.sumSiersc + " </td><td>" + wynik.sumUszy + " </td><td>" + wynik.sumUzebienie + " </td><td>" + wynik.sumPostawa + " </td><td>" + wynik.sumPazury + " </td></tr>");
        });
    });

});
