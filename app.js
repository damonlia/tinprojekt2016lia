var express = require('express');
var async = require('async');
var _ = require('underscore');
var app = express();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);
var zbiorSedziow = [];
var idsSedziow = [],
    liczbaSedziow = 5;
var tabFindObjPassword = [];
var port = process.env.PORT || 3000;

var database = require('./public/schema');

app.use('/lib/', express.static(__dirname + '/bower_components/jquery/dist/'));
app.use('/lib2/', express.static(__dirname + '/bower_components/underscore/'));
app.use(express.static(__dirname + '/public'));
//
// app.get('/oceniaj', function(req, res) {
//     res.sendFile(__dirname + '/public/judgeSite.html/');
// });
app.get('/admin', function(req, res) {
    res.sendFile(__dirname + '/public/adminSite.html');
});
app.get('/wyniki', function(req, res) {
    res.sendFile(__dirname + '/public/kittySite.html');
});

var i, j;
var wylosowaniSedziowie = [],
    wylosowaneKoty = [],
    selectHaslo;


console.log("Wylosowanekoty:" + wylosowaneKoty);
/////////////////////////////////////////////////////////
var haslaObiekty = [],
    idDanegoSedziego = [],
    hasla = [],
    idDanegoSedziegoinArray = [],
    kotyObiekty = [],
    koty = [],
    wszyscySedziowieObiekty = [],
    wszystkieHaslaSedziow = [],
    zalogowaniSedziowie = [],
    znajdzWszystkichSedziow, queryKoty;
async.series([
    function(cb) {
        znajdzWszystkichSedziow = database.Judge.find({}).select('haslo -_id');
        znajdzWszystkichSedziow.exec(function(err, judge) {
            wszyscySedziowieObiekty = judge;
            cb();
        });
    },
    function(cb) {
        while (wylosowaneKoty.length < 5) {
            var randomnumber = Math.floor(Math.random() * 10) + 1;
            var found = false;
            for (i = 0; i < wylosowaneKoty.length; i++) {
                if (wylosowaneKoty[i] == randomnumber) {
                    found = true;
                    break;
                }
            }
            if (!found) wylosowaneKoty[wylosowaneKoty.length] = randomnumber;
        }
        queryKoty = database.Kitty.find({
            'startNumber': {
                $in: wylosowaneKoty
            }
        }).select('_id');

        queryKoty.exec(function(err, kitty) {
            kotyObiekty = kitty;
            cb();
        });
    },
    function(cb) {
        koty = _.pluck(kotyObiekty, '_id');
        wszystkieHaslaSedziow = _.pluck(wszyscySedziowieObiekty, 'haslo');
        console.log("Tablica kotow: " + koty);
        console.log("Tablica hasel wszystkich sedziow: " + wszystkieHaslaSedziow);
        cb();
    }
]);
var kotOceniany = 0;
var nrK;
var socketIdAdmin;
///////////////////// Otworz polaczenie   //////////////////////////////////
console.log('otwieram polaczenie');
database.db.once('open', function() {
    io.sockets.on("connection", function(socket) {

        //////////////////////// sprawdz haslo    //////////////////////////////////
        socket.on('sprawdzHaslo', function(haslo) {
            console.log("przed sprawdzeniem");
            var zawieraHaslo, nrDanegoSedziegoinArray = [];
            zawieraHaslo = _.contains(wszystkieHaslaSedziow, haslo);
            powtarzaSieHaslo = _.contains(zalogowaniSedziowie, haslo);
            if (zawieraHaslo && !powtarzaSieHaslo) {
                async.series([
                    function(cb) {
                        zalogowaniSedziowie.push(haslo);
                        var queryZnajdzSedziegoHasle = database.Judge.find({
                            'haslo': haslo
                        }).select('judgeNumber _id');
                        queryZnajdzSedziegoHasle.exec(function(err, judge) {
                            console.log("1");
                            idDanegoSedziego = judge;
                            console.log(idDanegoSedziego);
                            cb();
                        });
                    },
                    function(cb) {
                        idDanegoSedziegoinArray = _.pluck(idDanegoSedziego, '_id');
                        nrDanegoSedziegoinArray = _.pluck(idDanegoSedziego, 'judgeNumber');
                        console.log("2");
                        console.log(idDanegoSedziegoinArray);
                        var idDanegoSedziego0 = idDanegoSedziegoinArray[0];
                        var nrSedziego = nrDanegoSedziegoinArray[0];
                        socket.emit('otworzFormularz', nrSedziego, idDanegoSedziegoinArray, koty[kotOceniany]);
                        idsSedziow.push({
                            'socketid': socket.id,
                            'haslo': haslo,
                            'idSedziego': idDanegoSedziego0.toString()
                        });
                        console.log("3");
                        console.dir(idsSedziow);
                        if (_.contains(hasla, haslo)) {
                            socket.emit('udostepnijFormularz', nrK);
                        }
                    }
                ]);
            } else {
                socket.emit('bledneHaslo', "Błędnie wpisane hasło, bądź nie masz odpowiednich uprawnień");
            }

        });
        ///////////////////////////////// Usun sedziego z listy zalogowanych   ///////////////////////////////
        socket.on('usunSedziegoZListyZalogowanych', function() {
            console.log("Przed usunieciem zalogowanego sedziego z listy:");
            console.dir(idsSedziow);
            var znalezionyObiektZalogowanegoSedziego = _.findWhere(idsSedziow, {
                'socketid': socket.id
            });
            if (typeof znalezionyObiektZalogowanegoSedziego !== 'undefined') {
                var index = idsSedziow.indexOf(znalezionyObiektZalogowanegoSedziego);
                idsSedziow.splice(index, 1);
                var index2 = zalogowaniSedziowie.indexOf(znalezionyObiektZalogowanegoSedziego.haslo);
                zalogowaniSedziowie.splice(index2, 1);
            }
            console.dir(idsSedziow);
            console.log("zalgowani Sedziowie po usunieciu: " + zalogowaniSedziowie);
        });
        ////////////////////////////////////////// rozpocznij pokaz   ///////////////////////////////////
        socket.on('rozpocznijPokaz', function() {
            var znalezionyObiekt;
            var znajdzNrStartowyKota = [],
                numerKotaObj = [],
                nrKota = [];
            while (wylosowaniSedziowie.length < 5) {
                var randomnumber = Math.floor(Math.random() * 10) + 1;
                var found = false;
                for (var i = 0; i < wylosowaniSedziowie.length; i++) {
                    if (wylosowaniSedziowie[i] == randomnumber) {
                        found = true;
                        break;
                    }
                }
                if (!found) wylosowaniSedziowie[wylosowaniSedziowie.length] = randomnumber;
            }
            console.log("Wylosowanie sedziowie" + wylosowaniSedziowie);
            async.series([
                function(cb) {
                    znajdzNrStartowyKota = database.Kitty.find({
                        '_id': koty[kotOceniany]
                    }).select('startNumber -_id').exec(function(err, nr) {
                        numerKotaObj = nr;
                    });
                    selectHaslo = database.Judge.find({
                        'judgeNumber': {
                            $in: wylosowaniSedziowie
                        }
                    }).select('haslo -_id');
                    selectHaslo.exec(function(err, judge) {
                        haslaObiekty = judge;
                        cb();
                    });
                },
                function(cb) {
                    nrKota = _.pluck(numerKotaObj, 'startNumber');
                    hasla = _.pluck(haslaObiekty, 'haslo');
                    nrK = nrKota[0];
                    console.log("Tablica hasel wylosowanych sedziow: " + hasla);
                    if (idsSedziow.length === 0) {
                        console.log("tablica idsSedziow(socketid oraz hasla zalogowanych sedziow) pusta");
                    } else {
                        for (i = 0; i < idsSedziow.length; i++) {
                            //szukam sedziów o hasle ktory zostal upoważniony do glosowania
                            znalezionyObiekt = _.findWhere(idsSedziow, {
                                'haslo': hasla[i]
                            });
                            if (typeof znalezionyObiekt !== 'undefined') {
                                //zapisuje do tablicy obiekt sedziego o hasle ktory moze glosowac
                                tabFindObjPassword.push(znalezionyObiekt);
                            }
                        }
                        console.log("6");
                        console.log(tabFindObjPassword);
                        if (tabFindObjPassword.length === 0) {
                            console.log("talica tabFindObjPassword pusta");
                        } else {
                            for (j = 0; j < tabFindObjPassword.length; j++) {
                                //dla kazdego sedziego na liscie udostepniam formularz
                                io.sockets.connected[tabFindObjPassword[j].socketid].emit('udostepnijFormularz', nrK);
                            }
                        }
                    }
                    cb();
                }
            ]);
        });

        socket.on('zakonczPokaz', function() {
            var idKotaOcenianegoObiekt = [],
                WszystkieOcenyObiekt = [],
                haslaSedziowObiektyNieocenione = [];
            var znajdzDokumentyZDanymKotem, selectIdKotaSedziegoOcenianego, execIdKotaOcenianego, selectWszytskieOceny, execWszystkieOceny, wezWszystkieKoty;
            var pluckSiersc = [],
                pluckUszy = [],
                pluckUzebienie = [],
                pluckPostawa = [],
                pluckPazury = [],
                wyniki = {},
                poszukujeImieKota = [],
                imieKota = [],
                pluckImiekota = [];
            var sumSiersc, sumUszy, sumUzebienie, sumPostawa, sumPazury, sumWszystko;
            async.series([
                function(cb) {
                    console.log("1");
                    znajdzDokumentyZDanymKotem = database.Result.find({
                        'kot_id': koty[kotOceniany]
                    }).and({
                        $or: [{
                            siersc: {
                                $exists: false
                            }
                        }, {
                            pazury: {
                                $exists: false
                            }
                        }, {
                            uzebienie: {
                                $exists: false
                            }
                        }, {
                            postawa: {
                                $exists: false
                            }
                        }, {
                            uszy: {
                                $exists: false
                            }
                        }]
                    });
                    console.log("2");
                    selectIdKotaSedziegoOcenianego = znajdzDokumentyZDanymKotem.select('kot_id sedzia_id -_id');
                    execIdKotaOcenianego = selectIdKotaSedziegoOcenianego.exec(function(err, result) {
                        idKotaOcenianegoObiekt = result;
                        console.log("Id kota aktualnie ocenianego, ktory nie ma wszytskich pol uzupelnionych " + idKotaOcenianegoObiekt);
                        cb();
                    });

                },
                function(cb) {
                    if (idKotaOcenianegoObiekt.length !== 0) {

                        var znalezionyObiektzIdSedziego, tabFindObjIDS = [];
                        //chce id sedziego kotry nie ocenil wszytskich kategorii
                        var idSedziegoOceniajacego = _.pluck(idKotaOcenianegoObiekt, 'sedzia_id');

                        for (i = 0; i < idSedziegoOceniajacego.length; i++) {
                            znalezionyObiektzIdSedziego = _.findWhere(idsSedziow, {
                                'idSedziego': idSedziegoOceniajacego[i].toString()
                            });

                            if (typeof znalezionyObiektzIdSedziego !== 'undefined') {
                                tabFindObjIDS.push(znalezionyObiektzIdSedziego);
                            }
                        }

                        if (tabFindObjIDS.length === 0) {
                            console.log("talica tabFindObjPassword pusta");
                        } else {
                            for (j = 0; j < tabFindObjIDS.length; j++) {
                                io.sockets.connected[tabFindObjIDS[j].socketid].emit('pospieszenie');
                            }
                        //    console.log("socket admina przy zakonczeniu: " + socketIdAdmin);
                        //    io.sockets.connected[socketIdAdmin].emit('pospieszanie');
                        }
                        cb();
                    } else {
                        console.log("5");
                        async.series([
                            function(cb) {
                                wezWszystkieKoty = database.Result.find({
                                    'kot_id': koty[kotOceniany]
                                });
                                selectWszytskieOceny = wezWszystkieKoty.select('siersc uszy postawa uzebienie pazury _id');
                                execWszystkieOceny = selectWszytskieOceny.exec(function(err, oceny) {
                                    WszystkieOcenyObiekt = oceny;
                                    console.log("Wszystkie oceny danego kota (obiekty)" + WszystkieOcenyObiekt);
                                    cb();
                                });
                            },
                            function(cb) {
                                poszukujeImieKota = database.Kitty.find({
                                    '_id': koty[kotOceniany]
                                }).select('name -_id').exec(function(err, imie) {
                                    imieKota = imie;
                                    console.log(imieKota);
                                    cb();
                                });
                            },
                            function(cb) {
                                pluckImiekota = _.pluck(imieKota, 'name');
                                pluckIdKota = _.pluck(WszystkieOcenyObiekt, '_id');
                                pluckSiersc = _.pluck(WszystkieOcenyObiekt, 'siersc');
                                console.log(pluckSiersc);
                                pluckUszy = _.pluck(WszystkieOcenyObiekt, 'uszy');
                                pluckUzebienie = _.pluck(WszystkieOcenyObiekt, 'uzebienie');
                                pluckPostawa = _.pluck(WszystkieOcenyObiekt, 'postawa');
                                pluckPazury = _.pluck(WszystkieOcenyObiekt, 'pazury');
                                cb();
                            },
                            function(cb) {

                                var iloscSedziow = WszystkieOcenyObiekt.length;
                                var reduce = function(pluck) {
                                    return (_.reduce(pluck, function(memo, num) {
                                        return memo + num;
                                    }, 0)) / iloscSedziow;
                                };
                                sumSiersc = reduce(pluckSiersc);
                                console.log(sumSiersc);
                                sumUszy = reduce(pluckUszy);
                                sumUzebienie = reduce(pluckUzebienie);
                                sumPostawa = reduce(pluckPostawa);
                                sumPazury = reduce(pluckPazury);
                                var wynikiOcen = [];
                                wynikiOcen.push(sumSiersc, sumUszy, sumUzebienie, sumPostawa, sumPazury);
                                sumWszystko = reduce(wynikiOcen);
                                cb();

                            },
                            function(cb) {
                                var imiekociaka = pluckImiekota[0];
                                var obiektWyniki = {
                                    sumSiersc: sumSiersc,
                                    sumUszy: sumUszy,
                                    sumUzebienie: sumUzebienie,
                                    sumPostawa: sumPostawa,
                                    sumPazury: sumPazury,
                                    sumWszystko: sumWszystko,
                                    imieKota: imiekociaka
                                };

                                console.dir(obiektWyniki);
                                io.sockets.emit('wyswietlTabele', obiektWyniki);
                                io.sockets.emit('zablokujFormularz');
                                wylosowaniSedziowie = [];
                                hasla = [];
                                haslaObiekty = [];
                                tabFindObjPassword = [];
                                kotOceniany++;
                                io.sockets.emit('nastepnyKot', koty[kotOceniany]);
                                cb();
                            }
                        ]);

                        cb();
                    }
                }
            ]);

        });

        socket.on('updateDoBazy', function(poleVal, idObecnegoSedziego, idObecnegoKota, poleWSchemacie) {
            console.log("Wartość ktora zostanie wpisana do bazy:" + poleVal);
            var zobaczCzyIstniejeDokument = {
                'sedzia_id': idObecnegoSedziego,
                'kot_id': idObecnegoKota
            };
            var obiektDoUpdate = {
                sedzia_id: idObecnegoSedziego,
                kot_id: idObecnegoKota,
            };
            obiektDoUpdate[poleWSchemacie] = poleVal;
            database.Result.findOneAndUpdate(zobaczCzyIstniejeDokument, obiektDoUpdate, {
                upsert: true
            }, function(err, doc) {
                if (err) {
                    return console.log(err);
                }
                console.log("succesfully saved");
            });

        });
        // socket.on('dajsocketid', function() {
        //     socketIdAdmin = socket.id;
        //     console.log("socket admina: " + socketIdAdmin);
        // });

    });


    httpServer.listen(port, function() {
        console.log('Serwer HTTP działa na porcie ' + port);
    });
});
